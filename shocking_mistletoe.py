__version__ = "0.1.0"
__version_info__ = (0, 1, 0)

import logging

import mistletoe
# from contrib.pygments_renderer import PygmentsRenderer
from shocking.plugin import BasePlugin

LOGGER = logging.getLogger("shocking")


class ShockingPlugin(BasePlugin):
    """ Markdown Parser using mistletoe """

    _defaults = {
        "extension": ".md",
    }

    def prepare(self):
        pass

    def process(self, file_object, **metadata):
        if file_object.path.suffix != self.config["extension"]:
            LOGGER.debug(f"File '{file_object.path}' is not a Markdown file. Skipping")
            return file_object

        return file_object._replace(
            content=mistletoe.markdown(file_object.content),
            path=file_object.path.with_suffix(".html")
        )
