from setuptools import setup

with open("README.rst", 'r') as file_object:
    long_description = file_object.read()

setup_kwargs = {
    'name': "shocking-mistletoe",
    'version': "0.1.0",
    'description': "shocking Plugin for the mistletoe Markdown Parser",
    'long_description': long_description,
    'long_description_content_type': "text/x-rst",
    'license': "MIT",
    'author': "Ted Moseley",
    'author_email': "tmoseley1106@gmail.com",
    'url': "https://gitlab.com/shocking/shocking-mistletoe",
    'project_urls': {
        "Bug Tracker": "https://gitlab.com/shocking/shocking-mistletoe/issues",
        # "Documentation": "https://docs.example.com/HelloWorld/",
        "Source Code": "https://gitlab.com/shocking/shocking-mistletoe",
    },
    'py_modules': ["shocking_mistletoe"],
    'install_requires': [
        "mistletoe>=0.7.1",
        "shocking==0.1.0",
    ],
    'entry_points': {
        'shocking.plugin': [
            "mistletoe = shocking_mistletoe:ShockingPlugin"
        ]
    },
    'platform': "any",
    'python_requires': ">=3.6",
    'classifiers': (
        "Development Status :: 1 - Planning",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Software Development :: Build Tools",
    ),
}


setup(**setup_kwargs)
