******************
shocking-mistletoe
******************

`shocking`_ Plugin for the `mistletoe`_ Markdown Parser

Configuration
=============

========= ============= =================================
Name      Default Value Description
========= ============= =================================
extension \".md\"       Extension used for Markdown files
========= ============= =================================

.. _`mistletoe`: https://github.com/miyuchina/mistletoe
.. _`shocking`: https://gitlab.com/tmose1106/shocking

Licensing
=========

The shocking package is licensed under the terms of the `MIT/Expat license`_.

.. _`MIT/expat license`: https://spdx.org/licenses/MIT.html
